import process from 'process'
import minimist from 'minimist'
import fs from 'fs'
import * as Name from 'w3name';

async function publish () {
  const args = minimist(process.argv.slice(2));
  const path = args.path;
  var key_file = args.key_file;
  if ( !key_file || key_file == "" || key_file == true ) {
      key_file = "priv.key"
  }
  if ( !path || path == "" || path == true ) {
    console.error("A CID path must be assigned to the name.");
    return 1;
  }
  if ( !args.update ) {
    const name = await Name.create();
    const revision = await Name.v0(name, "/ipfs/"+path);
    await fs.promises.writeFile(key_file, name.key.bytes);
    console.log("Private key stored in binary file:", key_file);
    await Name.publish(revision, name.key);
    console.log("Published name:", name.toString());
  }
  else {
    const bytes = await fs.promises.readFile(key_file);
    const name = Name.from(bytes);
    const revision = await Name.resolve(Name.parse((await name).toString()));
    const nextPath = "/ipfs/"+path;
    const nextRevision = await Name.increment(revision, nextPath);
    await Name.publish(nextRevision, (await name)._privKey);
    console.log("Published name:\n", (await name).toString(),
      "With new revision:\n", nextRevision.value);
  }
  
}

publish()
