This repository contains a set of helper tools to publish a w3name, push a new version of the w3name, query the latest version, etc.

example of use:  
```
# Will publish a w3name for the CID provided in path
/data # node publish-name.js --path=lalalalalalalalalalalalalalalalalalala
Private key stored in binary file: priv.key
Published name: dudududududududududududududududududududu
```
```
# Will update the w3name for a new path to point to. You need to provide the private key generated on the first example, to push the new version
/data # node publish-name.js --path=lalililililililillililalalalalala --update --key_file=".secret/meine.privkey"
```
